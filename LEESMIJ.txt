
Samenvatting:
Data van project073 wiki, beschikbaar als http://host/path/mysqldump.sql.gz
en rsync://host/website_code_bestanden, die wil je op andere server
gebruiken.


p073lugwiki-ansible probeert meerdere dingen te gelijk te doen:

 * een "restore" van de project073 LUG wiki te beschrijven
 * een Ansible toepassing te zijn
 * flexibel te zijn



De "restore" is het plaatsen van backup bestanden.

Het is beschreven door middel van scripts.
Om zo de beschrijving in een uitvoerbaar formaat te hebben.

Ansible is een orchestration tool.
Speel met de bestanden en scripts die hier zijn.
Lees meer over Ansible op bijvoorbeeld https://en.wikipedia.org/wiki/Ansible_%28software%29

Flexibel zit in niet een exacte restore uit te voeren, maar een rebuild.
Dus dat de "restore" op een andere plaats mogelijk is.


Aanwezige bestanden:
* parameters.yaml
	Configuratie items
	Is een deel van de flexibiliteit
	Gebruik 'parameters.yaml.ex' als beginpunt voor jouw versie

* inventory
	Tegen welke servers de "restore" / "rebuild" opdrachten
	afgevuurd worden. inventory.ex is een voorbeeld,
	eventueel nog inventory.geert

* ping
	Check tool om te zien dat Ansible connectie kan maken.

* mysql
	Om database objecten aan te maken.
	Het creert de wiki database en een wiki database user.

* wipemysql
	Om database spullen te wissen,
	ook een test op mysql toegang voor ansible.

* wbsrvr-*
	Om webservers in te richten

* *.vhost.j2
	Template files voor webserver vhost definitie

* php
	Om PHP op de webserver te installeren

* sourcelocations_*.yaml
	Configuratiebestand dat beschrijft waar gegevens staan.
	De plaatsen waar de "backup archives" liggen.

* databasecontent
	Vult database met inhoud van een database dump,
	die dump wordt eerst opgehaald.

* website
	Script wat de website code files overhaalt, "rsyncd".

* connect
	Verbindt website code met database. Maakt ook andere verbindingen,
	zoals webserver URL.


= Gebruik =

== Veronderstellende voorkennis ==

Er is de verwachting dat je bekend bent met een command line interface.

Dat je weet wat een "vhost" en "documentroot" is,
dat je weet wat er mee bedoelt is.

Dat je in kunt loggen op een database. ( MariaDB, MySQL )


== Voorbereiding ==

( een `git clone` van deze repository )

Heb je eigen <code>parameters.yaml</code> en <code>inventory</code>.

Heb <code>ansible</code> geinstalleerd ( apt install ansible )

Heb een server waar de "rebuild" ( "restore" ) moet gebeuren.
Dat mag ook je testlaptop zijn. Belangrijk is dat je er met Ansible bij kunt.

Heb voor die server DNS records.  Dan wel op andere manier geregelt dat
er name resolving is.  ( edit /etc/hosts )

Ook nog een database server. Mariadb of MySQL.
En daar een database user die een volgende database user mag aanmaken.

== Aan de slag ==

Nu de voorbereidingen gebeurd zijn, kun je aan de slag.

== ping ==

Controleer je ansible setup m.b.v. het commando

  ./ping

In de output moet dan 'ok' staan.

== wipemysql ==

En met
  ./wipemysql
kom je er achter dat <tt>python-mysql</tt> en <tt>~/.my.cnf</tt> goed zijn.

<tt>python-mysql</tt> is het Debian package waarmee Ansible bij MySQL kan.

Voor het zetten van <tt>~/.my.cnf</tt> kun het script dat staat
op <code>http://stappers.it/ma</code> gebruikt worden.


== PHP ==

Installeer eventueel nog PHP en nodige PHP libraries.

  ./php


== Webserver ==

Webserver inrichten kan de "wbsrvr-*" scripts, bijvoorbeeld

  ./wbsrv-minimal

Maar die is wel erg minimaal. Er zijn echter completere.
Overweeg om de webserver met de hand in te richten.
Andere overweging is om wel het juiste "wbsrvr-*" playbook te maken.
Bijvoorbeeld `wbsrv-nginx-php-fpm`

En met
  ./wbsrvr-nginx-php-fpm  --start-at-task='have vhost available'
installeer je alleen een VHOST voor nginx.

== Database stuff ==

Door

  ./mysql

wordt een database, Mariadb of MySQL, klaar gezet.

Met

  ./databasecontent

krijgt de database ook inhoud.
Want er wordt een databasedump ingelezen.

== Website ==

Voor upload van de website is

  ./website


Om website en database met elkaar te verbinden is er

  ./connect


Je hebt nu een clone van de project073 wiki.


= Verdere exprimenten =

Met je ''clone'' kun je exprimenteren.

Verversen van de omgeving doe je met

  ./databasecontent
  ./website
  ./connect


Heb je leuke ''extensies'' gevonden, laat het dan weten.
Bijvoorbeeld via de mailinglist van Project073
of stappers@stappers.it


# l l
