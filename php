#!/usr/bin/ansible-playbook --inventory=./inventory
#
#
---

- name: Deploy PHP stuff for Mediawiki
  gather_facts: yes
  hosts: webservers
  become: yes

  tasks:

  - name: Install Packages with Apt
    when: ansible_distribution == 'Debian' or ansible_distribution == 'Ubuntu'
    apt:
      name: "{{ item }}"
      state: present
      install_recommends: no
    with_items:
      - php
      - php-mysqli
      - php-mbstring
      - php-xml

  - name: Install Packages with Yum
    when: ansible_distribution == 'CentOS' or ansible_distribution == 'Red Hat Enterprise Linux'
    yum: name={{ item }}
      state=present
    with_items:
      - untested_package_names
      - php
      - php-mysqli
      - patches_welcome

# l l
