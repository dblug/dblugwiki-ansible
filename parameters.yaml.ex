#
# parameters
#
---

  httphost:
    name: w073.my.test.domain
    ## en bij een restore
    # name: www.dblug.nl
    port: 80
  documentroot:
    # location in the file system
    path: /srv/webserver/lugwiki073
    # which group should have write privilege
    group: adm
  database:
    server: localhost
    port: 3306
    schema: wiki073
    export: expwk073 # to export anonymouse e-mail addresses
    changepassword: on_create
    username: wuku073
    password: 370iKWy
    dumpdir: "{{ documentroot.path}}/dbd"
    # note: a database server doesn't need to have a "webserver doc root dir"
  #
  # iwp, Install Packages With  'apt' or 'yum'
  ipw: apt

  includesourceloc: ./sourcelocations_jheronimus.yaml

  # Found at https://www.mediawiki.org/wiki/Download
  mediawiki_url: https://releases.wikimedia.org/mediawiki/1.29/mediawiki-1.29.1.tar.gz

# l l
